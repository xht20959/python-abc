import dlib
from PIL import Image
import numpy as py
import os

file_path = "./people_i_know/figure_1.jpg"

detector = dlib.get_frontal_face_detector()  # 加载检测器
win = dlib.image_window()

image = py.array(Image.open(file_path))  # 读取图片
detector_face = detector(image, 1)  # 检测 1是放大图片

win.clear_overlay()
win.set_image(image)  # 显示图pain
win.add_overlay(detector_face)  # 添加检测框
dlib.hit_enter_to_continue()