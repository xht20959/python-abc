# 线性回归

## 1. 什么是线性回归?
什么是回归：从大量的函数结果和自变量反推回函数表达式的过程就是回归。线性回归是利用数理统计中回归分析来确定两种或两种以上变量间相互依赖的定量关系的一种统计分析方法。——主要是解决值预测的问题

一元线性回归：只包括一个自变量（x1）和一个因变量（y），且二者的关系可用一条直线近似表示，这种回归分析称为一元线性回归分析。公式：
$$
y = w_0 + w_1*x_1
$$
![1](./src/images/figure_1.png)

多元线性回归：如果回归分析中包括两个或两个以上的自变量，且因变量和自变量之间是线性关系，则称为多元线性回归分析。公式： 
$$
y = w_0 + w_1*x_1 + w_2*x_2 + w_3*x_3 + ... +  + w_n*x_n
$$
对于一元线性回归公式来说，如果给定两组x,y的值，就可以求得和的值。两点确定一条直线，可以确定和的值。w0我们可以叫做一元线性回归公式的截距，w1可以叫做一元线性回归公式的斜率。

![2](./src/images/figure_2.png)

监督学习是指：利用一组已知类别的样本调整 分类器 的 参数 ，使其达到所要求性能的过程，也称为 监督 训练或有教师学习。.  监督学习是从标记的训练数据来推断一个功能的机器学习任务。. 训练数据包括一套训练示例。.  在监督学习中，每个实例都是由一个输入对象（通常为矢量）和一个期望的输出值（也称为监督信号）组成。.  监督学习算法是分析该训练数据，并产生一个推断的功能，其可以用于映射出新的实例。.  一个最佳的方案将允许该算法来正确地决定那些看不见的实例的类标签。

![3](./src/images/figure_3.png)

监督学习（数据集有输入和输出数据）通过已有的一部分输入数据与输出数据之间的相应关系。生成一个函数，将输入映射到合适的输出，比如分类。
![4](./src/images/figure_4.png)

对新输入进行预测

![5](./src/images/figure_5.png)

在我们将要建模的房价预测问题里，$$x_i$$是描述房子的各种属性（比如房间的个数、周围学校和医院的个数、交通状况等），而$$y_i$$是房屋的价格。

![6](./src/images/figure_6.png)

房子实际卖价550万人民币

- 91平方米

- 4个卧房

```python
def estimate_house_value(floor_area_sqm, number_of_bedrooms):
    #基础价格
    b = 3000000

    #每平方米16000元
    #250000每个房间
    value = b + 16000 * floor_area_sqm + 250000 * number_of_bedrooms
    return value

estimate_value = estimate_house_value(91, 4)
print("房子估价：",estimate_value,"元")
```

```
房子估价： 5456000 元
```

以上为2元线性回归，模型具有固定的weights

![7](./src/images/figure_7.png)

![8](./src/images/figure_8.png)

## 2. 误差公式

误差公式： 取得最小值时，能确定一条完美的直线，也就是确定一组 值与自变量的关系，拟合出来的一条直线能尽可能的通过更多的点。确定了error的最小值，也就有可能确定一组值。

如何确定error的最小值？

如果将error函数看成自变量关于error的函数，那么可以转换为： 

![9](./src/images/figure_9.png)

如果将一组权重看成一列向量，表示一列维度组成的向量，就是这组向量的转置。那么现在的问题就是如何找到一组，使得error的值最小。我们可以看出error函数的关系图像大概是一个开口向上的图像，那么当error的导数为0时，此时能得到一组的值对应的error的值最小。

 ![10](./src/images/figure_10.png)

通过求导的方式，理论上可以确定error的最小值，但是由于是一组数据，无法确定error最小下对应的这一组权重 。正向的求导不能得到一组权重值，就不能确定线性回归的公式。那么可以根据数据集来穷举法反向的试，来确定线性回归的公式。
![11](./src/images/figure_11.png)

![12](./src/images/figure_12.png)

![13](./src/images/figure_13.png)

安装pandas和scikit-learn

```bash
pip install pandas
pip install scikit-learn
```

HousePrice.csv

```csv
Area_sqm,Price_RMB_M
75,4.35
90,5.76
87,5.52
65,4.12
46,3.78
89,5.59
102,6.12
76,4.26
120,6.83
118,6.55
```

demo.py

```python
#pip install pandas
#pip install scikit-learn
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn.linear_model import LinearRegression
df = pd.read_csv("HousePrice.csv")
print(df)
```

```
   Area_sqm  Price_RMB_M
0        75         4.35
1        90         5.76
2        87         5.52
3        65         4.12
4        46         3.78
5        89         5.59
6       102         6.12
7        76         4.26
8       120         6.83
9       118         6.55
```

```python
X  = df.iloc[:, 0]
print(X)
```

```
0     75
1     90
2     87
3     65
4     46
5     89
6    102
7     76
8    120
9    118
```

```python
X = df.iloc[:, 0].values
print(X)
```

```
[ 75  90  87  65  46  89 102  76 120 118]
```

```python
print(X.shape)
```

```
(10,)
```

```python
y = df.iloc[:,1].values
print(y)
```

```
[4.35 5.76 5.52 4.12 3.78 5.59 6.12 4.26 6.83 6.55]
```

```python
plt.scatter(X,y)
plt.title("House Price by Size")
plt.xlabel("House Area in square meter")
plt.ylabel("Selling Price in million RMB")
plt.show()
```

![14](./src/images/figure_14.png)

- 一元线性回归

![15](./src/images/figure_15.png)

![16](./src/images/figure_16.png)

- 多元线性回归

![17](./src/images/figure_17.png)

![18](./src/images/figure_18.png)

![19](./src/images/figure_19.png)

第二步：创建一个简单线性回归模型

```python
#pip install pandas
#pip install scikit-learn
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn.linear_model import LinearRegression
df = pd.read_csv("HousePrice.csv")
#print(df)
X = df.iloc[:, 0]
#print(X)
X = df.iloc[:, 0].values
#print(X)
#print(X.shape)
y = df.iloc[:,1].values
#print(y)
plt.scatter(X,y)
plt.title("House Price by Size")
plt.xlabel("House Area in square meter")
plt.ylabel("Selling Price in million RMB")
#plt.show()
lr = LinearRegression()
X = X.reshape(-1, 1)
print(X,y)
model = lr.fit(X,y)
print(lr.predict(X))
```

